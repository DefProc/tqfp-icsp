TQFP-ISP
========

Circuit to provide the crystal and ISCP connections for flashing ATMega328P-AU chips before soldering. 

Connects to the zero insertion socket (ZIF) socket on a 32 pin DIP breakout supplied my [Adafruit](http://adafru.it/1240).
