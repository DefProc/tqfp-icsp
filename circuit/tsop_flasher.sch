EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:tsop_flasher-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATMEGA328-A IC1
U 1 1 56B36FD9
P 5100 3300
F 0 "IC1" H 4350 4550 50  0000 L BNN
F 1 "ATMEGA328-A" H 5500 1900 50  0000 L BNN
F 2 "Housings_DIP:DIP-32_W15.24mm" H 5100 3300 50  0000 C CIN
F 3 "" H 5100 3300 50  0000 C CNN
	1    5100 3300
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR01
U 1 1 56B3700D
P 3900 1900
F 0 "#PWR01" H 3900 1750 50  0001 C CNN
F 1 "+5V" H 3900 2040 50  0000 C CNN
F 2 "" H 3900 1900 50  0000 C CNN
F 3 "" H 3900 1900 50  0000 C CNN
	1    3900 1900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 56B37023
P 3900 4850
F 0 "#PWR02" H 3900 4600 50  0001 C CNN
F 1 "GND" H 3900 4700 50  0000 C CNN
F 2 "" H 3900 4850 50  0000 C CNN
F 3 "" H 3900 4850 50  0000 C CNN
	1    3900 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 4500 4200 4500
Wire Wire Line
	4200 4400 3900 4400
Connection ~ 3900 4500
Wire Wire Line
	3900 2300 4200 2300
Wire Wire Line
	3900 1900 3900 2300
Wire Wire Line
	4200 2200 3900 2200
Connection ~ 3900 2200
NoConn ~ 4200 2500
NoConn ~ 4200 2800
NoConn ~ 4200 3550
NoConn ~ 4200 3650
$Comp
L Crystal_Small Y1
U 1 1 56B37079
P 7400 2850
F 0 "Y1" H 7400 2950 50  0000 C CNN
F 1 "16MHz" H 7400 2750 50  0000 C CNN
F 2 "Crystals:HC-49V" H 7400 2850 50  0001 C CNN
F 3 "" H 7400 2850 50  0000 C CNN
	1    7400 2850
	0    -1   -1   0   
$EndComp
Text GLabel 6200 2500 2    60   Input ~ 0
MOSI
Text GLabel 6500 2600 2    60   Input ~ 0
MISO
Text GLabel 6800 2700 2    60   Input ~ 0
SCK
Text GLabel 6600 3650 2    60   Input ~ 0
RST
Wire Wire Line
	6100 2500 6200 2500
Wire Wire Line
	6100 2600 6500 2600
Wire Wire Line
	6100 2700 6800 2700
Wire Wire Line
	6100 3650 6600 3650
$Comp
L +5V #PWR03
U 1 1 56B371CB
P 6350 3300
F 0 "#PWR03" H 6350 3150 50  0001 C CNN
F 1 "+5V" H 6350 3440 50  0000 C CNN
F 2 "" H 6350 3300 50  0000 C CNN
F 3 "" H 6350 3300 50  0000 C CNN
	1    6350 3300
	1    0    0    -1  
$EndComp
$Comp
L R_Small R1
U 1 1 56B371E3
P 6350 3450
F 0 "R1" H 6380 3470 50  0000 L CNN
F 1 "10K" H 6380 3410 50  0000 L CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" H 6350 3450 50  0001 C CNN
F 3 "" H 6350 3450 50  0000 C CNN
	1    6350 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3300 6350 3350
Wire Wire Line
	6350 3550 6350 3650
Connection ~ 6350 3650
NoConn ~ 6100 2200
NoConn ~ 6100 2300
NoConn ~ 6100 2400
NoConn ~ 6100 3050
NoConn ~ 6100 3150
NoConn ~ 6100 3250
NoConn ~ 6100 3350
NoConn ~ 6100 3450
NoConn ~ 6100 3550
NoConn ~ 6100 3800
NoConn ~ 6100 3900
NoConn ~ 6100 4000
NoConn ~ 6100 4100
NoConn ~ 6100 4200
NoConn ~ 6100 4300
NoConn ~ 6100 4400
NoConn ~ 6100 4500
$Comp
L GND #PWR04
U 1 1 56B37372
P 7950 3150
F 0 "#PWR04" H 7950 2900 50  0001 C CNN
F 1 "GND" H 7950 3000 50  0000 C CNN
F 2 "" H 7950 3150 50  0000 C CNN
F 3 "" H 7950 3150 50  0000 C CNN
	1    7950 3150
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 56B3738C
P 7750 2700
F 0 "C1" H 7760 2770 50  0000 L CNN
F 1 "22" H 7650 2600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 7750 2700 50  0001 C CNN
F 3 "" H 7750 2700 50  0000 C CNN
	1    7750 2700
	0    1    1    0   
$EndComp
$Comp
L C_Small C2
U 1 1 56B373C3
P 7750 3000
F 0 "C2" H 7760 3070 50  0000 L CNN
F 1 "22" H 7760 2920 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 7750 3000 50  0001 C CNN
F 3 "" H 7750 3000 50  0000 C CNN
	1    7750 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	7850 2700 7950 2700
Wire Wire Line
	7950 2700 7950 3150
Wire Wire Line
	7850 3000 7950 3000
Connection ~ 7950 3000
Wire Wire Line
	6100 2800 7150 2800
Wire Wire Line
	7150 2800 7150 2700
Wire Wire Line
	7150 2700 7650 2700
Wire Wire Line
	7400 2750 7400 2700
Connection ~ 7400 2700
Wire Wire Line
	6100 2900 7150 2900
Wire Wire Line
	7150 2900 7150 3000
Wire Wire Line
	7150 3000 7650 3000
Wire Wire Line
	7400 2950 7400 3000
Connection ~ 7400 3000
Text GLabel 2650 3250 2    60   Input ~ 0
MOSI
Text GLabel 1750 3050 1    60   Input ~ 0
MISO
Text GLabel 1650 3250 0    60   Input ~ 0
SCK
Text GLabel 1750 3450 3    60   Input ~ 0
RST
$Comp
L +5V #PWR05
U 1 1 56B3773A
P 2550 3050
F 0 "#PWR05" H 2550 2900 50  0001 C CNN
F 1 "+5V" H 2550 3190 50  0000 C CNN
F 2 "" H 2550 3050 50  0000 C CNN
F 3 "" H 2550 3050 50  0000 C CNN
	1    2550 3050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 56B37754
P 2550 3450
F 0 "#PWR06" H 2550 3200 50  0001 C CNN
F 1 "GND" H 2550 3300 50  0000 C CNN
F 2 "" H 2550 3450 50  0000 C CNN
F 3 "" H 2550 3450 50  0000 C CNN
	1    2550 3450
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X03 P1
U 1 1 56B37770
P 2150 3250
F 0 "P1" H 2150 3450 50  0000 C CNN
F 1 "ICSP_HEADERS" H 2150 3050 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_2x03" H 2150 2050 50  0001 C CNN
F 3 "" H 2150 2050 50  0000 C CNN
	1    2150 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 3050 1750 3150
Wire Wire Line
	1750 3150 1900 3150
Wire Wire Line
	1650 3250 1900 3250
Wire Wire Line
	1750 3450 1750 3350
Wire Wire Line
	1750 3350 1900 3350
Wire Wire Line
	2550 3050 2550 3150
Wire Wire Line
	2550 3150 2400 3150
Wire Wire Line
	2650 3250 2400 3250
Wire Wire Line
	2550 3450 2550 3350
Wire Wire Line
	2550 3350 2400 3350
Wire Wire Line
	3900 4400 3900 4850
NoConn ~ 4200 4300
$EndSCHEMATC
